============
OGRA library
============

OGRA is a javascript library that allows display of online charts using
several third party libraries using one unified format.


DOCUMENTATION
=============

Online documentation (also included in this repository):
    https://labs.pages.labs.nic.cz/ogra/

OGRA is used for statistics of Czech domain name registry:
    https://stats.nic.cz/


BUG REPORTS
===========

Please reports any bugs at https://gitlab.labs.nic.cz/labs/ogra/issues.

PROJECT SITE
============

Official site of the project is https://gitlab.labs.nic.cz/labs/ogra.

LICENCE
=======

This library is released under the terms of BSD License and was developed by
CZ.NIC Labs - research and development department of CZ.NIC association - top
level domain registy for .CZ. Copy of the BSD License is distibuted along with
this library.

